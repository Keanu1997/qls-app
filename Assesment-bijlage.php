<?php

// MyQLS API docs: https://api.pakketdienstqls.nl/swagger/


// In de bedrijfspraktijk zal dit uit een webshop API voortkomen, nu even als statische data.
$order = [
   'number' => '#958201',
   'billing_address' => [
      'companyname' => 'My Great Company B.V.',
      'name' => '',
      'street' => 'Bamendawg',
      'housenumber' => '18',
      'address_line_2' => '',
      'zipcode' => '3319GS',
      'city' => 'Dordrecht',
      'country' => 'NL',
      'email' => 'emailaddress@example.org',
      'phone' => '0101234567',
   ],
   'delivery_address' => [
      'companyname' => '',
      'name' => 'John Doe',
      'street' => 'Bamendawg',
      'housenumber' => '18',
      'address_line_2' => '',
      'zipcode' => '3319GS',
      'city' => 'Dordrecht',
      'country' => 'NL',
   ],
   'order_lines' => [
      [
         'amount_ordered' => 2,
         'name' => 'Jeans - Black - 36',
         'sku' => 69205,
         'barcode' =>  'JB36',
      ],
      [
         'amount_ordered' => 1,
         'name' => 'Sjaal - Rood Oranje',
         'sku' => 25920,
         'barcode' =>  'SJA2940291',
      ]
   ]
];

$emailAdd = 'frits@test.qlsnet.nl';
$password = '4QJW9yh94PbTcpJGdKz6egwH';

$companyId = '9e606e6b-44a4-4a4e-a309-cc70ddd3a103';
$brandId = 'e41c8d26-bdfd-4999-9086-e5939d67ae28';

// @TODO Fetch possible shipping products https://api.pakketdienstqls.nl/company/{$companyId}/product

// @TODO Let the user choose the shipping product (for example DHL Pakje, DHL Brievenbuspakje); Now hardcoded DHL Pakje (NL)
$productId = 2;
// @TODO Let the user choose the shipping product options (combination) (for example DHL Pakje without options, with signature, no neightbour delivery, DHL Brievenbuspakje without options); Now hardcoded DHL Pakje (NL) without delivery options
$productCombinationId = 3;

$ch = curl_init("https://api.pakketdienstqls.nl/company/{$companyId}/shipment/create");
curl_setopt($ch, CURLOPT_HTTPHEADER, [
	'Accept: application/json',
	'Content-Type: application/json'
]);
curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode([
	'brand_id' => $brandId,
	'reference' => $order['number'],
	'weight' => 1000,
	'product_id' => $productId,
	'product_combination_id' => $productCombinationId, 
	'cod_amount' => 0,
	'piece_total' => 1,
	'receiver_contact' => [
		'companyname' => $order['delivery_address']['companyname'],
		'name' => $order['delivery_address']['name'],
		'street' => $order['delivery_address']['street'],
		'housenumber' => $order['delivery_address']['housenumber'],
      'postalcode' => $order['delivery_address']['zipcode'],
		'locality' => $order['delivery_address']['city'],
		'country' => $order['delivery_address']['country'],
		'email' => $order['billing_address']['email'],
	]
]));
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
curl_setopt($ch, CURLOPT_USERPWD, $emailAdd . ':' . $password);  
curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

$response = curl_exec($ch);
curl_close($ch);

echo '<pre>';
print_r($response);

// @TODO Tip: Convert pdf label to image, for easy inclusion in next step

// @TODO Generate delivery packing slip (pakbon) add generated shipping label to be included in the file