@extends('layouts')
@section('content')

    <div id="alert_section">

    </div>

    <form>
        <div class="mb-3">
            <label for="shipping_product" class="form-label">Shipping product</label>
            <div id="input_shipping_products_section">

            </div>
        </div>
        <div class="mb-3">
            <label class="form-label">Shipping Combination</label>
            <div id="input_shipping_combinations_section">

            </div>
        </div>
        <button type="button" class="btn btn-primary">Submit</button>
    </form>
@endsection

@push('foot')
    <script type="text/javascript">
        const selectShippingProduct = document.getElementById('shipping_product');
        const rowAlert = document.getElementById('alert_section');
        const inputSectionProducts = document.getElementById('input_shipping_products_section');
        const inputSectionCombinations = document.getElementById('input_shipping_combinations_section');

        let combinations = new Map();

        const loadProducts = () => {
            inputSectionProducts.innerHTML = `<i class="spinner-border spinner-border-sm text-primary mr-1"></i> Loading..`;
            inputSectionCombinations.innerHTML = `<i class="spinner-border spinner-border-sm text-primary mr-1"></i> Loading..`;

            const companyId = 1;

            fetch('/api/order/create')
            .then(response => {
                if (!response.ok) throw new Error(`Something went wrong while fetching the products.`);
                return response.json()
            })
            .then(json => {
                const selectList = document.createElement('select');
                selectList.id = 'shipping_products';
                selectList.name = 'shipping_products';

                inputSectionProducts.innerHTML = ``;
                inputSectionProducts.appendChild(selectList);

                for(const product in json.products) {
                    const option = document.createElement('option');
                    option.value =  product.id;
                    option.text = product.name;

                    selectList.appendChild(option);

                    const items = new Map();
                    for (const combination in product) {
                        items.set(combination.id, combination.value);
                    }

                    combinations.set(product.id, items);
                }
            })
            .catch(error => {
                rowAlert.innerHTML = `
            <div class="alert alert-danger" role="alert">
                ${error.message}
            </div>
            `;
            });
        };

        const loadCombinations = () => {
            const currentProduct = selectShippingProduct.value;
            const options = combinations.get(currentProduct);
            const selectList = document.createElement('select');

            inputSectionCombinations.innerHTML = `<i class="spinner-border spinner-border-sm text-primary mr-1"></i> Loading..`;

            inputSectionCombinations.appendChild(selectList);

            options.forEach((value, key) => {
                const option = document.createElement('option');
                option.value = key;
                option.text = value;

                selectList.appendChild(option);
            });
        };

        document.addEventListener('DOMContentLoaded', function() {
            loadProducts();
        });

        document.addEventListener('change', (e) => {
            if(e.target && e.target.id == 'shipping_products') loadCombinations();
        })
    </script>
@endpush