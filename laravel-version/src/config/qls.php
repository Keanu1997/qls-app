<?php

return [
    'api'   => [
        'url'   => env('QLS_API_URL'),
        'auth'  => [
            'username'  => env('QLS_API_AUTH_USERNAME'),
            'password'  => env('QLS_API_AUTH_PASSWORD')
        ]
    ]
];