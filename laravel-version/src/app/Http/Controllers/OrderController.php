<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreOrderRequest;
use App\Services\CreateShipment;
use App\Services\FetchAvailableShippingProducts;
use App\Services\GeneratePackingSlip;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    /**
     * @return JsonResponse
     */
    public function create(): JsonResponse
    {
        $shippingProducts = (new FetchAvailableShippingProducts)->run();

        dd($shippingProducts);

        return response()->json(['shippingProducts' => $shippingProducts]);
    }

    public function store(StoreOrderRequest $request)
    {
        $order = (new CreateShipment($request->product_id, $request->product_combination_id))->run();
        return (new GeneratePackingSlip($order))->run();
    }
}
