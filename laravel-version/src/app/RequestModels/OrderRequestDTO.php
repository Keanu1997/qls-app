<?php

namespace App\RequestModels;

use Spatie\DataTransferObject\DataTransferObject;

class OrderRequestDTO extends DataTransferObject
{
    public string $brandId;
    public string $reference;
    public int $weight;
    public int $productId;
    public int $productCombinationId;
    public int $codAmount;
    public int $pieceTotal;
    public ReceiverContactRequestDTO $receiverContact;
}