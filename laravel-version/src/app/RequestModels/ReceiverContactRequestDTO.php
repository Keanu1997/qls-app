<?php

namespace App\RequestModels;

use Spatie\DataTransferObject\DataTransferObject;

class ReceiverContactRequestDTO extends DataTransferObject
{
    public string $companyName;
    public string $name;
    public string $street;
    public int $housenumber;
    public string $postalcode;
    public string $locality;
    public string $country;
    public string $email;
}