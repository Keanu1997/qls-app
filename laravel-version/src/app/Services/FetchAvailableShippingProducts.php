<?php

namespace App\Services;

use App\Gateways\QlsApiGateway;

class FetchAvailableShippingProducts
{
    private QlsApiGateway $gateway;
    private string $companyId = '9e606e6b-44a4-4a4e-a309-cc70ddd3a103';

    public function __construct()
    {
        $this->gateway = new QlsApiGateway($this->companyId);
    }

    public function run()
    {
        return $this->gateway->getAvailableShippingProducts();
    }
}