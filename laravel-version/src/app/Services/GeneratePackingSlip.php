<?php

namespace App\Services;

use App\ResponseModels\OrderResponseDTO;
use Illuminate\Support\Facades\Storage;
use Spatie\PdfToImage\Pdf;

class GeneratePackingSlip
{
    private OrderResponseDTO $order;

    public function __construct(OrderResponseDTO $order)
    {
        $this->order = $order;
    }

    public function run()
    {
        $arrContextOptions=array(
            "ssl"=>array(
                "verify_peer"=>false,
                "verify_peer_name"=>false,
            ),
        );

        $reference = $this->order->reference;
        $labelPDF = file_get_contents($this->order->label->url, false, stream_context_create($arrContextOptions));

        Storage::disk('local')->put("/pdf/$reference.pdf", $labelPDF);

        $filePath = Storage::disk('local')->path("/pdf/$reference.pdf");

        $labelImage = new Pdf($filePath);
        $labelImage->saveImage(Storage::disk('local')->path('/image/test.jpg'));
        
        dd("hello world");

        // $pdf = app()->make('dompdf.wrapper');
        // $pdf->loadView('pdf.packing-slip', ['order' => $this->order]);
        
        // return $pdf->download('packingslib.pdf');
    }
}