<?php

namespace App\Services;

use App\Entities\Label;
use App\Gateways\QlsApiGateway;
use App\RequestModels\OrderRequestDTO;
use App\RequestModels\ReceiverContactRequestDTO;
use App\ResponseModels\LabelResponseDTO;
use App\ResponseModels\OrderResponseDTO;
use App\ResponseModels\ReceiverContactResponseDTO;

class CreateShipment
{
    private QlsApiGateway $gateway;
    private string $companyId = '9e606e6b-44a4-4a4e-a309-cc70ddd3a103';
    private int $productId;
    private int $productCombinationId;

    public function __construct(int $productId, int $productCombinationId)
    {
        $this->gateway = new QlsApiGateway($this->companyId);
        $this->productId = $productId;
        $this->productCombinationId = $productCombinationId;
    }

    public function run(): OrderResponseDTO
    {
        $label = $this->gateway->createShipment($this->setOrderRequestDTO());
        
        return $this->setOrderResponseDTO($label);
    }

    private function setOrderRequestDTO(): OrderRequestDTO
    {
        $order = $this->getOrderMock();

        return new OrderRequestDTO([
            'brandId'               => $order->brandId,
            'reference'             => $order->reference,
            'weight'                => $order->weight,
            'productId'             => $this->productId,
            'productCombinationId'  => $this->productCombinationId,
            'codAmount'             => $order->codAmount,
            'pieceTotal'            => $order->pieceTotal,
            'receiverContact'       => new ReceiverContactRequestDTO([
                'companyName'   => $order->receiverContact->companyName,
                'name'          => $order->receiverContact->name,
                'street'        => $order->receiverContact->street,
                'housenumber'   => $order->receiverContact->housenumber,
                'postalcode'    => $order->receiverContact->postalcode,
                'locality'      => $order->receiverContact->locality,
                'country'       => $order->receiverContact->country,
                'email'         => $order->receiverContact->email
        ])
        ]);
    }

    private function setOrderResponseDTO(Label $label): OrderResponseDTO
    {
        $order = $this->getOrderMock();

        return new OrderResponseDTO([
            'brandId'               => $order->brandId,
            'reference'             => $order->reference,
            'weight'                => $order->weight,
            'productId'             => $this->productId,
            'productCombinationId'  => $this->productCombinationId,
            'codAmount'             => $order->codAmount,
            'pieceTotal'            => $order->pieceTotal,
            'receiverContact'       => new ReceiverContactResponseDTO([
                'companyName'   => $order->receiverContact->companyName,
                'name'          => $order->receiverContact->name,
                'street'        => $order->receiverContact->street,
                'housenumber'   => $order->receiverContact->housenumber,
                'postalcode'    => $order->receiverContact->postalcode,
                'locality'      => $order->receiverContact->locality,
                'country'       => $order->receiverContact->country,
                'email'         => $order->receiverContact->email
            ]),
            'label'                 => new LabelResponseDTO([
                'url'               => $label->getUrl()
            ])
        ]);
    }

    private function getOrderMock(): object
    {
        return (object)[
            'brandId'               => 'e41c8d26-bdfd-4999-9086-e5939d67ae28',
            'reference'             => '#958201',
            'weight'                => 1000,
            'codAmount'             => 0,
            'pieceTotal'            => 1,
            'receiverContact'       => (object)[
                'companyName'   => '',
                'name'          => 'John Doe',
                'street'        => 'Bamendawg',
                'housenumber'   => '18',
                'postalcode'    => '3319GS',
                'locality'      => 'Dordrecht',
                'country'       => 'NL',
                'email'         => 'emailaddress@example.org'
            ]
        ];
    }
}