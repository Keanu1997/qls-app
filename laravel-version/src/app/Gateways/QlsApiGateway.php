<?php

namespace  App\Gateways;

use App\Entities\Label;
use App\Entities\ShippingProduct;
use App\Entities\ShippingProductCombination;
use App\RequestModels\OrderRequestDTO;
use Illuminate\Support\Facades\Http;

class QlsApiGateway
{
    private string $url;
    private array $options;
    private string $username;
    private string $password;
    private string $companyId;

    public function __construct(string $companyId, array $options = [])
    {
        $this->url = config('qls.api.url');
        $this->username = config('qls.api.auth.username');
        $this->password = config('qls.api.auth.password');
        $this->companyId = $companyId;
        $this->options = $options;
    }

    /**
     * return ShippingProduct[]
     */
    public function getAvailableShippingProducts(): array
    {
        $shippingProducts = [];

        $response = Http::withBasicAuth($this->username, $this->password)
        ->get($this->url . '/company/' . $this->companyId . '/product')->object();

        foreach($response->data as $product)
        {
            $combinations = $this->getAvailableShippingProductCombinations($product->combinations);
            $shippingProduct = new ShippingProduct();
            $shippingProduct->setId($product->id);
            $shippingProduct->setName($product->name);
            $shippingProduct->setCombinations($combinations);

            $shippingProducts[] = $shippingProduct;
        }

        return $shippingProducts;
    }

    public function createShipment(OrderRequestDTO $data): Label
    {
        $urlComplete = $this->url . '/company/' . $this->companyId . '/shipment/create';
        $receiverContact = $data->receiverContact;

        $response = Http::withBasicAuth($this->username, $this->password)
        ->withHeaders([
            'Accept' => 'application/json',
            'Content-Typ'   => 'application/json'
        ])->post($urlComplete, [
            'brand_id'                  => $data->brandId,
            'reference'                 => $data->reference,
            'weight'                    => $data->weight,
            'product_id'                => $data->productId,
            'product_combination_id'    => $data->productCombinationId, 
            'cod_amount'                => $data->codAmount,
            'piece_total'               => $data->pieceTotal,
            'receiver_contact'          => [
                'companyname'   => $receiverContact->companyName,
                'name'          => $receiverContact->name,
                'street'        => $receiverContact->street,
                'housenumber'   => $receiverContact->housenumber,
                'postalcode'    => $receiverContact->postalcode,
                'locality'      => $receiverContact->locality,
                'country'       => $receiverContact->country,
                'email'         => $receiverContact->email
            ]
        ]);

        $label = new Label();
        $label->setUrl($response->object()->data->labels->a4->offset_0);

        return $label;
    }

    /**
     * @param array
     * @return ShippingProductCombination[]
     */
    private function getAvailableShippingProductCombinations(array $combinations): array
    {
        $shippingProductCombinations = [];

        foreach($combinations as $combination)
        {
            $productCombination = new ShippingProductCombination();
            $productCombination->setId($combination->id);
            $productCombination->setName($combination->name);

            $shippingProductCombinations[] = $productCombination;
        }

        return $shippingProductCombinations;
    }
}