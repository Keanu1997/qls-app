<?php

namespace App\Entities;

class ShippingProduct
{
    private int $id;
    private string $name;
    private array $combinations;

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getCombinations(): array
    {
        return $this->combinations;
    }

    /**
     * return ShippingProductCombination[]
     */
    public function setCombinations(array $combinations): void
    {
        $this->combinations = $combinations;
    }
}