<?php

namespace App\ResponseModels;

use Spatie\DataTransferObject\DataTransferObject;

class LabelResponseDTO extends DataTransferObject
{
    public string $url;
}