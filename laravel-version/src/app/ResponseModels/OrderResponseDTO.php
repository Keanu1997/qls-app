<?php

namespace App\ResponseModels;

use Spatie\DataTransferObject\DataTransferObject;

class OrderResponseDTO extends DataTransferObject
{
    public string $brandId;
    public string $reference;
    public int $weight;
    public int $productId;
    public int $productCombinationId;
    public int $codAmount;
    public int $pieceTotal;
    public ReceiverContactResponseDTO $receiverContact;
    public LabelResponseDTO $label;
}